"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders

Modified by: Abduarraheem Elfandi
Project 4: Brevet time calculator with Ajax
CIS 322
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

# Brevets = [
#             200, 
#             300, 
#             400, 
#             600, 
#             1000
# ]

# Table format (min location, max location, min speed, max speed). 
Table = [
         (0, 200, 15, 34), 
         (200, 400, 15, 32), 
         (400, 600, 15, 30), 
         (600, 1000, 11.428, 28), 
         (1000, 1300, 13.333, 26)
]

Min_speed_times = {
                  200: 13.5, 
                  300: 20, 
                  400: 27, 
                  600: 40, 
                  1000: 75
}

def open_time(control_dist_km, brevet_dist_km, brevet_start_time, table=Table):
   """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
   """ 
   brevet_start_time = arrow.get(brevet_start_time) # change ths starting time to be arrow type so we can shift the time.
   if control_dist_km == 0: # opening time is 0 when control location is 0.
      return brevet_start_time.shift(hours= 0, minutes= 0).isoformat()

   if control_dist_km > brevet_dist_km: # if the control location is more than the brevet.
      control_dist_km = brevet_dist_km # just set control to be the brevet since control location is slightly longer than brevet location is irrelvant.
   time = 0
   remainder_km = control_dist_km
   for entry in table:
      
      # check if the control location is in the range, 
      # if it is then calculate the opening time
      if entry[0] <= control_dist_km <= entry[1]: 
         time += remainder_km / entry[3]
         break
      
      # the control location is beyond this range
      # so for controls beyond the first brevet the max speed
      # decreases so we need to take that into account.
      else:
         time += table[0][1] / entry[3]
         remainder_km -= table[0][1] #my guess is that it should be remainder_km-= (upper range - lower range) but its not really specified, so pretty much subtract by 200

   # convert value to hours and minutes
   hours = int(time)
   mins = round((time - hours) * 60)
   closeTime = brevet_start_time.shift(hours= hours, minutes= mins)
   return closeTime.isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time, table=Table, min_speed_times=Min_speed_times):
   """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
   """

   brevet_start_time = arrow.get(brevet_start_time) # change ths starting time to be arrow type so we can shift the time.
   if control_dist_km == 0: # closing time for the control at 0 is 1.
      return brevet_start_time.shift(hours= 1, minutes= 0).isoformat()

   time = 0 
   remainder_km = control_dist_km
   for entry in table:
      # if the control location is beyond or equal to the our brevet location, 
      # the calulate the closing time based on the closest brevet.
      if control_dist_km >= brevet_dist_km:
         closest = min(min_speed_times.items(), key=lambda x: abs(control_dist_km-x[0])) # will return the closest value to control_dist_km.
         time = closest[1]
         break

      # control location is in the range so calculate closing time.
      elif entry[0] <= control_dist_km <= entry[1]:
         time += remainder_km / entry[2]
         break
      # the control location is beyond this range
      # so for controls beyond the first brevet the min speed
      # decreases so we need to take that into account,
      # in this case the first 3 ranges use the same min speed,
      # but the 600-1000 range doesn't so this needs to be done
      else:
         time += table[0][1] / entry[2]
         remainder_km -= table[0][1] # pretty much subtract by 200.

   # convert value to hours and minutes       
   hours = int(time)
   mins = round((time - hours) * 60)
   closeTime = brevet_start_time.shift(hours= hours, minutes= mins)
   return closeTime.isoformat()
